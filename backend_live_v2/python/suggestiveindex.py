# coding: utf-8

# In[17]:

from os import popen
import requests
import subprocess
import time
import urllib
from elasticsearch import Elasticsearch
import json 
from datetime import datetime
from pymongo import MongoClient
from pprint import pprint
from gingerit.gingerit import GingerIt


production_ip = '52.77.59.43'
#elasticsearch_server_address = '/home/ec2-user/routes/elastic_search/ElasticSearch/elasticsearch-2.2.1'
#def start_server():
#    try:
#        subprocess.Popen(elasticsearch_server_address)
#        time.sleep(10)
#        res = requests.get("http://localhost:9200")
#        print res
#    except Exception(e):
#        pass
#    
def get_elastic_search():
    es = Elasticsearch([{'host':'localhost', 'port':9200}])
    return es

#start_server()
es = get_elastic_search()
SUGGESTIVE_INDEX_NAME = 'suggestive_phrases'
SUGGESTIVE_DATA       = 'suggestive-data'

def create_suggestive_word():
    
    global SUGGESTIVE_INDEX_NAME
    global SUGGESTIVE_DATA
    if es.indices.exists(SUGGESTIVE_INDEX_NAME):
        es.indices.delete(index = SUGGESTIVE_INDEX_NAME)
    mapping = {
      "mappings" : {
        'suggestive-data' : {
            "properties":{
                "name": {"type":"text"},
                "suggest": { "type": "completion"}
            }
        }
      }   
    }
    es.indices.create(index = SUGGESTIVE_INDEX_NAME,ignore = 400,body = mapping,request_timeout = 6000)


def index_suggestive_word(filename):
    
    global SUGGESTIVE_INDEX_NAME
    global SUGGESTIVE_DATA
    if es.indices.exists(SUGGESTIVE_INDEX_NAME):
       es.indices.delete(index = SUGGESTIVE_INDEX_NAME)

    i = 0
    with open(filename, 'r') as f:
         for line in f:
             words = line.split()

             data = {
                "name": line.rstrip(),
                "suggest": {"input" : [words[0],words[1]]}
             }
             
             res = es.index(index = SUGGESTIVE_INDEX_NAME, doc_type = SUGGESTIVE_DATA, id = i, body = data)
             i += 1
             

create_suggestive_word()            
#index_suggestive_word("C:/Users/Lenovo/Documents/Pensieve - API/api_server/python/2_grams_test.txt")







            

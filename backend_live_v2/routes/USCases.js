/**
 * Created by pradeep on 04/05/17.
 */
var moment = require('moment');
var mongo = require('mongodb');
var assert = require('assert');
var request = require("request")
const util = require("util")
var cases = require("./cases");
var Server = mongo.Server,
    Db = mongo.Db,
    BSON = mongo.BSONPure;

//var mongoHost = "52.77.250.60"; //staging
var mongoHost = "52.77.59.43"; //production

var dbName = "pensieve";
var collection = "us_data";
    var collection_corpus_id = "case_id_source"

var server = new Server(mongoHost, 27017, {auto_reconnect: true});

db = new Db(dbName, server); //test

db.open(function(err, db) {

    if(!err) {

        console.log("Connected to 'cases' database");

        db.authenticate('pensieve_user', 'Pixel%0909', function(err, result) {
            
            assert.equal(true, result);
            
        });

        db.collection(collection, {strict:true}, function(err, collection) {

            if (err) {

                console.log("The 'us_data' collection doesn't exist") ;

            }

        });

    }

});

exports.findCasesByCaseIds = function(req, res) {
    
    var id = req.body;
    var idsProjects  = id["cases"];

    db.collection(collection, function(err, collection) {

        if (err) {

            throw err;
            console.log("error");
            res.send({"Error":"Error occurred on server"});

        } else {

            collection.find({"case_id_source": {$in:idsProjects}}).toArray(function(err, docs){ //, function(err, item) {

                if(!err) {
                    
                    docs.sort(function(a, b) {

                        return idsProjects.indexOf(a.case_id_source) - idsProjects.indexOf(b.case_id_source);

                    });

                    res.send(docs);

                } else {

                    res.send({"err : " : err});

                }

            });
        }

    });

};


exports.findSimilarByCaseId = function(req, res) {

    var case_id = req.body['case_id'];
    console.log(case_id);
    request.post('http://52.77.161.203:8000/us_similarybycaseid',{ json: { 'case_id': case_id } },function (error, response, body) {

            if (!error && response.statusCode == 200) {

                var corpus_ids =[];
                var relevance_array= [];
                var corpus_dict = {};

                for (i=0; i<body['case'].length; i++) {

                    corpus_ids.push(body['case'][i][0]);
                    relevance_array.push(body['case'][i][1]);
                    corpus_dict[body['case'][i][0]] = body['case'][i][1];           // here jsonObject['sync_contact_list'][i] is your current "bit"

                }
		console.log(corpus_dict);

                if(corpus_ids.length>0){

                    db.collection('us_data', function(err, collection) {

                        if (err) {

                            throw err;

                        } else {
			    collection.find({'case_id_source':case_id}).toArray(function(err,request_case){
				console.log(request_case[0].case_id_source);
				collection.find({'corpus_id': {$in:corpus_ids}}).toArray(function(err, item){

                                	item.sort(function(a, b) {

	                                    return parseFloat(b.relevancy) - parseFloat(a.relevancy);
	
        	                        });
	
        	                        res.send({"request_case":request_case[0],"similar_cases":item});

                            	});	
			    })
                            

                        }

                    });

                } else {

                    res.send({"error":"Error from server"});

                }

            } else {
		console.log(error);
		console.log(response);
		console.log(body);
                res.send({"error":"Error from server"});

            }

        });

};


exports.findSimilarByText = function(req, res) {

    var search_query = req.body.postbody.search_query;
    
    request.post('http://52.77.161.203:8000/similarCasesByUSText',{ json: { 'search_query': search_query } },function (error, response, body) {

            if (!error && response.statusCode == 200) {

                var corpus_ids =[];
                var relevance_array= [];
                var corpus_dict = {};

                for (i=0; i<body['case'].length; i++) {

                    corpus_ids.push(body['case'][i][0]);
                    relevance_array.push(body['case'][i][1]);
                    corpus_dict[body['case'][i][0]] = body['case'][i][1];           // here jsonObject['sync_contact_list'][i] is your current "bit"

                }

                if(corpus_ids.length>0){

                    db.collection(collection, function(err, collection) {

                        if (err) {

                            throw err;

                        } else {

                            collection.find({'corpus_id': {$in:corpus_ids}}).toArray(function(err, item) {

                                if(err){

                                    throw err;

                                } else {

                                    
                                    for(j=0; j<item.length;j++){

                                            item[j]['relevancy'] = corpus_dict[item[j]['corpus_id']];

                                    }

                                    item.sort(function(a, b) {

                                        return parseFloat(b.relevancy) - parseFloat(a.relevancy);

                                    });

                                    cases.add_feedback(search_query, item, "lda_model", function(result){

                                        res.send({"search_id":result, "search_query":search_query,"similar_cases":item});

                                    });

                                }

                            });

                        }

                    });

                    db.collection('my_account', function(err, collection) {

                      if(err)  {

                        throw err;

                      }
                      else{

                        var searchObject = {'search_query': search_query, timestamp: moment().format()};                        

                        collection.findOneAndUpdate({'email': req.body.username},{$push:{searchedQueries:searchObject}},function(err, item){

                            if(err){

                                throw err;

                            }
                            else{

                                console.log(item);
                            }

                        });

                      }

                    });

                } else{

                    res.send({"error":"No similar results found"});

                }

            } else{
		console.log(error);
                res.send({"error":response});

            }

    });

};


exports.similarUSTextWordcloud = function(req, res) {
	var search_query = req.body['search_query'];
	request.post(
		'http://52.77.161.203:8000/similar_by_text_us_wordcloud',
		{ json: { 'search_query': search_query } },
		function (error, response, body) {
			if (!error && response.statusCode == 200) {
				var filters=response.body.word_cloud;
				var similar_cases=response.body.similar_cases;

				var lenCorpus=similar_cases.length;
				var filters_corpusids={};
				var corpus_ids=[];

				for(i=0;i<lenCorpus;i++) {
					var filter_topics=similar_cases[i].words_from_topic;
					var lentopic=filter_topics.length;
					for(j=0;j<lentopic;j++) {
						if(filters_corpusids[filter_topics[j]]==undefined) {
							filters_corpusids[filter_topics[j]]=[];    
						}
						filters_corpusids[filter_topics[j]].push(similar_cases[i].corpus_id);
					}
				}

				//Convert corpus to case
				var objCases={};
				var lenObj=Object.keys(filters_corpusids).length;

				db.collection(collection, function(err, collection) {
					if (err) {
						throw err;
					} else{
						for(filter_data in filters_corpusids) {
							var corpus_ids=filters_corpusids[filter_data];
							if(corpus_ids.length>0) {
								filtercallback(collection,corpus_ids,objCases,filter_data, function(callback) {
									var lencallback=Object.keys(callback).length;
									if(lencallback==lenObj) {
										res.send({'filters':filters,'filter_cases':callback});
										//console.log(callback);
									}
								});
							}
						}
					}
				});
			}
			else{
				res.send({"error":error});
			}
		}
	);
}
filtercallback=function(collection,corpus_ids,objCases,filter_data,callback) {
	console.log('params');
	collection.find({'corpus_id': {$in:corpus_ids}}).toArray(function(err, item) {
		if (err) {
			throw err;
		} else {
			if(objCases[filter_data]==undefined) {
				objCases[filter_data]=[];
			}
			var itemlen=item.length;
			for(j=0; j<itemlen;j++){
				objCases[filter_data].push(item[j]['case_id']);
			}
			callback(objCases);
		}
	});    
}

exports.displayFilteredData=function(req,res) {
	var search_query = req.body['search_query'];
	var filters_query = req.body['filters'];
	var arrFilters = filters_query.split("&");
	var lenFilters=arrFilters.length;
	var corpus_dict = {};
	request.post(
		'http://52.77.161.203:3000/similar_by_text_wordcloud',
		{ json: { 'search_query': search_query } },
		function (error, response, body) {
			//console.log('response==='+JSON.stringify(response));
			if (!error && response.statusCode == 200) {
				//console.log('response=='+JSON.stringify(response));	
				var corpus_ids=[];
				var similar_cases = response.body.similar_cases;
				var lenSimilarCases = similar_cases.length;
				//console.log('response=='+JSON.stringify(similar_cases));	
				for(i=0;i<lenSimilarCases;i++) {
					var add=1;
					var topics=similar_cases[i].words_from_topic;
					for(j=0;j<lenFilters;j++) {
						if(arrFilters[j].indexOf(topics)!=-1) {
							add=0;
							break;
						}
					}
					if(add==1) {
						corpus_ids.push(similar_cases[i].corpus_id);
					}
				}
				console.log(corpus_ids);
				
				if(corpus_ids.length>0){
					db.collection(collection_corpus_id, function(err, collection) {
						if (err) {
							throw err;
						} else{
							collection.find({'corpus_id': {$in:corpus_ids}}).toArray(function(err, item){ //, function(err, item) {
								if (err) {
									throw err;
								} else {
									//console.log(corpus_dict[corpus_ids[0]   ]);
									for(j=0; j<item.length;j++){
										item[j]['relevancy'] = corpus_dict[item[j]['corpus_id']];
									}
									//it.set('relevancy', corpus_dict[item[0]['corpus_id']] );
									item.sort(function(a, b) {
										return parseFloat(b.relevancy) - parseFloat(a.relevancy);
									});
									var search_id;
									
									var feedback_json = { 
											"search_query": search_query, 
											"search_result": item
									};
									collection.insert(feedback_json, function(err, result) {
										if (err) {
											console.log("error "+err);
											res.send({"search_id":search_id,"search_query":search_query,"similar_cases":item});
											//callback ({'error':'Error in add_feedback'});
										} else {
											console.log('Success: ' + feedback_json._id);
											//callback(feedback_json._id);
											search_id = feedback_json._id;
											res.send({"search_id":search_id,"search_query":search_query,"similar_cases":item});
											//return feedback_json._id
										}
									});
								}
								//res.send({"search_id":search_id,"search_query":search_query,"similar_cases":item});
							});
						}
					});
				} else{
					res.send({"error":"No similar results found"});
				}
			}
		});
}


var mongo = require('mongodb');
var assert = require('assert');
var request = require("request");
const util = require("util");
var cases = require("./cases");
var moment = require("moment");
var Server = mongo.Server,
    Db = mongo.Db,
    BSON = mongo.BSONPure;

//var mongoHost = "52.77.250.60"; //staging
var mongoHost = "52.77.59.43"; //production

var dbName = "pensieve";
var collection = "delhi_hc_cases";
// var collection_corpus_id = "delhi_case_corpus_index"

var server = new Server(mongoHost, 27017, {auto_reconnect: true});

db = new Db(dbName, server); //test

db.open(function(err, db) {
    if(!err) {
        console.log("Connected to 'cases' database");
        db.authenticate('pensieve_user', 'Pixel%0909', function(err, result) {
            assert.equal(true, result);
            console.log("database connection established");
            //db.close();
        });
        db.collection(collection, {strict:true}, function(err, collection) {
            if (err) {
                console.log("The 'delhi_hc_cases' collection doesn't exist") ;
            }
        });

    }
});


exports.findCasesByCaseIds = function(req, res) {
    console.log("delhi findCasesByCaseIds");
    var id = req.body;
    //console.log('Retrieving case: ' + id);
    var idsProjects  = id['cases'];
    console.log("req "+ req + " id "+ id + " idprojects "+idsProjects);

    //var b = idsProjects.split(',').map(Number);
    db.collection(collection, function(err, collection) {
        if (err) {
            throw err;
            console.log("error");
            res.send({"Error":"Error occurred on server"});
        } else {
            collection.find({'corpus_id': {$in:idsProjects}}).toArray(function(err, docs){ //, function(err, item) {
                //collection.findOne({'case_id': Number(id), function(err, item) {
                docs.sort(function(a, b) {
                    //                        // Sort docs by the order of their _id values in ids.
                    return idsProjects.indexOf(a.case_id) - idsProjects.indexOf(b.case_id);
                });
                res.send(docs);
            });
        }
    });
};


var request = require('request');
exports.findSimilarByCaseId = function(req, res) {
    var case_id = req.body['case_id'];
    console.log("body "+ case_id);
    //var dbh = db;
    request.post(
        'http://52.77.161.203:3000/delhi_similarybycaseid',
        { json: { 'case_id': case_id } },
        function (error, response, body) {

            if (!error && response.statusCode == 200) {
                var corpus_ids =[];
                var relevance_array= [];
                var corpus_dict = {};
                for (i=0; i<body['case'].length; i++) {
                    corpus_ids.push(body['case'][i][0]);
                    relevance_array.push(body['case'][i][1]);
                    corpus_dict[body['case'][i][0]] = body['case'][i][1];
                    // here jsonObject['sync_contact_list'][i] is your current "bit"
                }
                console.log(corpus_ids);
                if(corpus_ids.length>0){
                    db.collection(collection_corpus_id, function(err, collection) {
                        if (err) {
                            throw err;
                        } else{
                            collection.find({'corpus_id': {$in:corpus_ids}}).toArray(function(err, item){ //, function(err, item) {

                                console.log('total cases ' + item.length);
                                /*for(j=0; j<item.length;j++){
                                 //item[j]['relevancy'] = corpus_dict[item[j]['corpus_id']];
                                 if(constants.DUPLICATES_SCR.indexOf(item[j]['case_id']) > 0){
                                 item.splice(j, 1);
                                 j--;
                                 }else{
                                 item[j]['relevancy'] = corpus_dict[item[j]['corpus_id']];
                                 }
                                 }
                                 console.log('total cases ' + item.length)*/

                                //it.set('relevancy', corpus_dict[item[0]['corpus_id']] );
                                item.sort(function(a, b) {
                                    return parseFloat(b.relevancy) - parseFloat(a.relevancy);
                                });
                                res.send({"request_case":case_id,"similar_cases":item});
                            });
                        }
                    });
                }else {
                    res.send({"error":"Error from server"});
                }
            }else{
                res.send({"error":"Error from server"});
            }
        });
};


exports.findSimilarByText = function(req, res) {
    var search_query = req.body.postbody.search_query;
    console.log("body "+ search_query);
    request.post(
        'http://52.77.161.203:3000/similarCasesByDelhiText',
        { json: { 'search_query': search_query } },
        function (error, response, body) {
            if (!error && response.statusCode == 200) {
                console.log(util.inspect(body, false, null));
                var corpus_ids =[];
                var relevance_array= [];
                var corpus_dict = {};
                for (i=0; i<body['case'].length; i++) {
                    corpus_ids.push(body['case'][i][0]);
                    relevance_array.push(body['case'][i][1]);
                    corpus_dict[body['case'][i][0]] = body['case'][i][1];
                    // here jsonObject['sync_contact_list'][i] is your current "bit"
                }
                console.log(corpus_ids);
                if(corpus_ids.length>0){
                    db.collection(collection, function(err, collection) {
                        if (err) {
                            throw err;
                        } else {
                            collection.find({'corpus_id': {$in:corpus_ids}}).toArray(function(err, item) { //, function(err, item) {
                                if(err){
                                    throw err;
                                }else{
                                    console.log('total cases ' + item.length);
                                    for(j=0; j<item.length;j++){
                                            item[j]['relevancy'] = corpus_dict[item[j]['corpus_id']];
                                    }
                                    item.sort(function(a, b) {
                                        return parseFloat(b.relevancy) - parseFloat(a.relevancy);
                                    });
                                    cases.add_feedback(search_query, item, "lda_model", function(result){
                                        console.log("query is being sent successful");
                                        res.send({"search_id":result, "search_query":search_query,"similar_cases":item})
                                    });
                                }
                            });
                        }
                    });
                    db.collection('my_account', function(err, collection) {
                      if(err)  {
                        throw err;
                      }
                      else{
                        var searchObject = {'search_query': search_query, timestamp: moment().format()};                        
                        collection.findOneAndUpdate({'email': req.body.username},{$push:{searchedQueries:searchObject}}, function(err, item){
                            if(err){
                                throw err;
                            }
                            else{
                                console.log(item);
                            }
                        });
                      }
                    });
                } else{
                    res.send({"error":"No similar results found"});
                }
            } else{
                res.send({"error":response});
            }
        });
};

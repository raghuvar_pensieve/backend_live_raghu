var mongo = require('mongodb');
var assert = require('assert');
var request = require("request")
const utils = require("util");
var moment = require("moment");
var Server = mongo.Server,
    Db = mongo.Db,
    BSON = mongo.BSONPure;

//var mongoHost = "52.77.250.60"; //staging
var mongoHost = "52.77.59.43"; //production

var dbName = "pensieve";
var collection = "taxman_db";
var collection_corpus_id = "corpus_id"

var server = new Server(mongoHost, 27017, {auto_reconnect: true});

db = new Db(dbName, server); //test

db.open(function(err, db) {
    if(!err) {
        console.log("Connected to 'cases' database");
        db.authenticate('pensieve_user', 'Pixel%0909', function(err, result) {
            assert.equal(true, result);

            //db.close();
        });
        db.collection(collection, {strict:true}, function(err, collection) {
            if (err) {
                console.log("The 'taxman_db' collection doesn't exist") ;
            }
        });

    }
});


exports.findCasesByTaxIds = function(req, res) {
    console.log(" in tax ids wala methods >>>>");
    var id = req.body;
    //console.log('Retrieving case: ' + id);
    var idsProjects  = id['cases'];
    console.log("req "+ req + " id "+ id + " idprojects "+idsProjects);

    //var b = idsProjects.split(',').map(Number);
    db.collection(collection, function(err, collection) {

        if (err) {
            throw err;
            console.log("error");
            res.send({"Error":"Error occurred on server"});
        } else {
            /*var idsProjects = [
             1999,
             2000 ];*/
           // collection.find({'corpus_id': {$in:idsProjects}}).toArray(function(err, item){ //, function(err, item) {
                //collection.findOne({'case_id': Number(id), function(err, item) {
             //   console.log("items are >> ........");
               // console.log(utils.inspect(item, false, null)) ;
                //res.send(item);
           // });

		collection.find({'corpus_id': {$in:idsProjects}}).toArray(function(err, docs){ //, function(err, item) {
            //collection.findOne({'case_id': Number(id), function(err, item) {
                            docs.sort(function(a, b) {
            //                        // Sort docs by the order of their _id values in ids.
                                                       return idsProjects.indexOf(a.case_id) - idsProjects.indexOf(b.case_id);
                         });
                res.send(docs);
        });
        }
    });
};



// Finds similar cases from input Text
 exports.findSimilarByText = function(req, res) {
    var search_query = req.body.postbody.search_query;
    console.log(utils.inspect(req.body, false, null)) ;
    console.log("search query :: " + search_query);
    console.log("In tax cases wala function");
    request.post(
       'http://52.77.161.203:3000/similarCasesByTaxText',
//         'http://localhost:3002/similarCasesByTaxText',
        { json: { 'search_query': search_query } },
        function (error, response, body) {
            if (!error && response.statusCode == 200) {
                console.log("response :: " + response);
                var corpus_ids =[];
//                var relevance_array= [];
                var corpus_dict = {};
                for (i=0; i<body['case'].length; i++) {
                    corpus_ids.push(body['case'][i][0]);
//                    relevance_array.push(body['case'][i][1]);
//                    corpus_dict[body['case'][i][0]] = body['case'][i][1];
                    // here jsonObject['sync_contact_list'][i] is your current "bit"
                }
                console.log(corpus_ids);
                response = {"search_query":search_query,"similar_cases":corpus_ids}
                console.log(utils.inspect(response, false, null)) ;
                res.send(response);
                db.collection('my_account', function(err, collection) {
                      if(err)  {
                        throw err;
                      }
                      else{
                        var searchObject = {'search_query': search_query, timestamp: moment().format()};                        
                        collection.findOneAndUpdate({'email': req.body.username},{$push:{searchedQueries:searchObject}}, function(err, item){
                            if(err){
                                throw err;
                            }
                            else{
                                console.log(item);
                            }
                        });
                      }
                });
            } else{
                res.send({"error":response});
            }
        });
}



exports.findSimilarByTaxId = function(req, res) {
    var case_id = req.body.case_id
    console.log("body "+ case_id)
    //var dbh = db;
    request.post(
        'http://52.77.161.203:3000/tax_similaritybycaseid',
        { json: { 'case_id': case_id } },
        function (error, response, body) {

            if (!error && response.statusCode == 200) {
                var corpus_ids =[];
                var relevance_array= [];
                var corpus_dict = {};
                for (i=0; i<body['case'].length; i++) {
                    corpus_ids.push(body['case'][i][0]);
                    relevance_array.push(body['case'][i][1]);
                    corpus_dict[body['case'][i][0]] = body['case'][i][1];
                    // here jsonObject['sync_contact_list'][i] is your current "bit"
                }
                console.log(corpus_ids);
                if(corpus_ids.length>0){
                    db.collection(collection, function(err, collection) {
                        if (err) {
                            throw err;
                        } else{
                            collection.find({'corpus_id': {$in:corpus_ids}}).toArray(function(err, item){ //, function(err, item) {

                                console.log('total cases ' + item.length)
                                /*for(j=0; j<item.length;j++){
                                 //item[j]['relevancy'] = corpus_dict[item[j]['corpus_id']];
                                 if(constants.DUPLICATES_SCR.indexOf(item[j]['case_id']) > 0){
                                 item.splice(j, 1);
                                 j--;
                                 }else{
                                 item[j]['relevancy'] = corpus_dict[item[j]['corpus_id']];
                                 }
                                 }
                                 console.log('total cases ' + item.length)*/

                                //it.set('relevancy', corpus_dict[item[0]['corpus_id']] );
                                item.sort(function(a, b) {
                                    return parseFloat(b.relevancy) - parseFloat(a.relevancy);
                                });
                                res.send({"request_case":case_id,"similar_cases":item});
                            });
                        }
                    });
                }else {
                    res.send({"error":"Error from server"});
                }
            }else{
                res.send({"error":"Error from server"});
            }
        });
}

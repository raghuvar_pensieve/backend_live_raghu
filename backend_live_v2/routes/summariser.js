/**
 * Created by pradeep on 18/03/17.
 */

var request = require("request");
const utils = require("util");

exports.summarise = function(req, res){
    var text = req.body.notes;
    request.post(
        'http://52.77.161.203:3000/summarise',
        {json :{'notes' : text}},
        function(error, response, body){
            if(!error && response.statusCode == 200){
                summarised = body['summary'];
                console.log("summarised text :: " + summarised);
                response = {"summary" : summarised};
                res.send(response);
            }
            else{
                res.send({"error" : response});
            }
        }
    );
}